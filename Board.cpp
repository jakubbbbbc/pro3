#include "Board.h"

Board::Board(){
    gameOver=false;

    counter=0;
    enSpeed=0;
    score=0;
    highScore=0;

    p.setPos(BoardWidth/2);

    dirCount=2;
    for (int i=1; i<BoardHeight-1; ++i)
        for (int j=1; j<BoardWidth-1; ++j)
            if (1==j%2 && 1==i%2 && i>1 &&i<8 && j>1 && j<BoardWidth-2)
                enemies[i][j]=1;
            else
                enemies[i][j]=0;
}

void Board::display(){
    for (int i=0; i<sizeY; ++i){
        for (int j=0; j<sizeX; ++j){
            if (i==0 || i==sizeY-1 || j==0 || j==sizeX-1)
                std::cout<<'#';
           // else if (i==sizeY-2 && j==p.getPos())
             //   std::cout<<'W';
            else
                std::cout<<view[i][j];
        }
        std::cout<<'\n';
    }
    std::cout<<"your score: "<<score<<std::endl;
    std::cout<<"your high score: "<<highScore<<std::endl;
}

void Board::moveEnemies(){
    //moving right
    if (1==dirCount || 2==dirCount || 3==dirCount){
        for (int i=BoardHeight-2; i>2; --i)
            for (int j=BoardWidth-2; j>1; --j)
                enemies[i][j]=enemies[i][j-1];
    ++dirCount;
    }
    //right edge
    else if (4==dirCount){
        for (int i=BoardHeight-1; i>2; --i)
            for (int j=1; j<BoardWidth; ++j){
                enemies[i][j]=enemies[i-1][j+1];
                if (BoardHeight-2==i && 1==enemies[i][j])
                    gameOver=true;
            }
    ++dirCount;
    }
    //moving left
    else if (5==dirCount || 6==dirCount || 7==dirCount){
        for (int i=BoardHeight-1; i>2; --i)
            for (int j=1; j<BoardWidth; ++j)
                enemies[i][j]=enemies[i][j+1];
    ++dirCount;
    }
    //left edge
    else if (8==dirCount){
        for (int i=BoardHeight-1; i>2; --i)
            for (int j=BoardWidth-2; j>0; --j){
                enemies[i][j]=enemies[i-1][j-1];
                if (BoardHeight-2==i && 1==enemies[i][j])
                    gameOver=true;
            }
    dirCount=1;
    }
}

void Board::enemyShoots(){
    for (int j=1; j<BoardWidth-1; j++)
        for (int i=BoardHeight-3; i>3; i--)
            if (1==enemies[i][j]){
                int pom=rand()%10;
                if (0==pom%10)
                    enShots[i+1][j]=1;
                break;
            }
}

void Board::moveShots(){
    for (int j=1; j<BoardWidth-1; j++)
        for (int i=BoardHeight-2; i>3; i--){
            enShots[i][j]=enShots[i-1][j];
            if (1==enShots[i][j] && ('^'==view[i][j] || '^'==view[i-1][j])){
                enShots[i][j]=0;
                enShots[i-1][j]=0;
                enShots[i+1][j]=0;
            }
            else if (1==enShots[i][j] && BoardHeight-2==i && j==p.getPos())
                gameOver=true;
        }
}

void Board::update(){
    int numOfEnemies=0;
    for (int i=1; i<sizeY-1; ++i)
        for (int j=1; j<sizeX-1; ++j)
            if (1==enemies[i][j]){
                view[i][j]='X';
                ++numOfEnemies;
            }
            else if (1==enShots[i][j])
                view[i][j]='U';
            else
                view[i][j]=' ';
    if (0==numOfEnemies)
        resetBoard(0);


    Node<Missile>* temp=missiles.head;
    //for (int i=0; i<missiles.getListSize(); ++i){
    while (temp!=NULL){
        temp->el->moveUp();
        if (temp->el->collided() || missileHit(temp->el)){
            Node<Missile>* prev=temp;
            temp=temp->next;
            missiles.deleteNode(prev);
            //--i;
        }
        else {
            view[temp->el->getPosY()][temp->el->getPosX()]='^';
            temp=temp->next;
        }
        /*if (temp->next)
            temp=temp->next;
        else
            break;*/
        //delete temp;
    }

    int currSpeed=10-enSpeed;
    if (1==counter%(currSpeed)){
        moveEnemies();
    }
    ++counter;
    int pom=rand()%(currSpeed)+1;
    if (1==currSpeed%pom){
        enemyShoots();
    }

    moveShots();

    view[sizeY-2][p.getPos()]='W';
}

bool Board::missileHit(Missile* m){
    if (1==enemies[m->getPosY()][m->getPosX()]){
        enemies[m->getPosY()][m->getPosX()]=0;
        view[m->getPosY()][m->getPosX()]=' ';
        score+=10*(enSpeed+1);
        if (score>highScore)
            highScore=score;
        return true;
    }
    return false;
}

void Board::resetBoard(int pom){
    gameOver=false;
    ++enSpeed;
    dirCount=2;
    for (int i=1; i<BoardHeight-1; ++i)
        for (int j=1; j<BoardWidth-1; ++j)
            if (1==j%2 && 1==i%2 && i>1 &&i<8 && j>1 && j<BoardWidth-2)
                enemies[i][j]=1;
            else
                enemies[i][j]=0;
    if (1==pom){
        score=0;
        enSpeed=0;
        counter=0;
        gameOver=false;
        missiles.clearList();
        for (int i=1; i<BoardHeight-1; ++i)
            for (int j=1; j<BoardWidth-1; ++j)
                enShots[i][j]=0;
    }
}

bool Board::getGameOver(){
    return gameOver;
}
