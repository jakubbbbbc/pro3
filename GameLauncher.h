#ifndef GAMELAUNCHER_H
#define GAMELAUNCHER_H

#include <windows.h>
#include "board.h"

void beforeGame();
void afterGame();
void startGame();

#endif

