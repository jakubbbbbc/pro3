#ifndef MISSILE_H
#define MISSILE_H

#include<iostream>

class Missile{
    int posX;
    int posY;

public:
    Missile(int, int);
    friend bool operator != (const Missile &c1, const Missile &c2);
    friend std::ostream& operator<< (std::ostream& os, const Missile& );
    int getPosX();
    int getPosY();
    void moveUp();
    bool collided();
};

#endif


