#ifndef PLAYER_H
#define PLAYER_H

class Player{
    int pos;
    int health;

public:
    Player();
    void setPos(int);
    int getPos();
    void makeMove(int);
};

#endif

