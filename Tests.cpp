#include "tests.h"

void Tests::declarationTest(){
    //int
    Fifo<int>* f1= new Fifo<int>();
    assert (f1->head==NULL && f1->tail==NULL && f1->getListSize()==0);
    delete f1;
    //char
    Fifo<char>*f2= new Fifo<char>();
    assert (f2->head==NULL && f2->tail==NULL && f2->getListSize()==0);
    delete f2;
    //long long
    Fifo<long long>*f3= new Fifo<long long>();
    assert (f3->head==NULL && f3->tail==NULL && f3->getListSize()==0);
    delete f3;
}

void Tests::addGetTest(){
    Fifo<long> f;
    //add
    long el=3456;
    f.addNode(&el);
    assert(*(f.head->el)==el && *(f.tail->el)==el && f.getListSize()==1);
    //get
    f.getNode();
    assert (f.head==NULL && f.tail==NULL && f.getListSize()==0);

    delete &f;
}

void Tests::clearTest(){
    Fifo<int> f;
    int t1[5]={1,2,3,4,5};
    for (int i=0; i<5; i++)
        f.addNode(&t1[i]);
    f.clearList();
    assert(f.getListSize()==0 && f.head==NULL && f.tail==NULL);
}

void Tests::deleteTest(){
    Fifo<int> f;
    int t1[5]={1,2,3,4,5};
    for (int i=0; i<5; i++)
        f.addNode(&t1[i]);
}

void Tests::missileTest(){
    Fifo<Missile>* f = new Fifo<Missile>;
    assert(f->head == NULL);
    Missile* m1 = new Missile (1,2);
    Missile* m2 = new Missile (3,4);
    Missile* m3 = new Missile (5,6);
    Missile* m4 = new Missile (7,8);
    Missile* m5 = new Missile (9,10);
    Missile* m6 = new Missile (11,12);

    f->addNode(m1);
    f->addNode(m2);
    f->addNode(m3);
    f->addNode(m4);
    f->addNode(m5);
    f->addNode(m6);
    assert(6==f->listSize);

    Missile* m = new Missile (3,4);
    Node<Missile>* n = new Node<Missile>(m);
    f->deleteNode(n);
    assert(5==f->listSize);
    f->getNode();
    assert(4==f->listSize);

    delete m;
    delete n;
    delete f;
}
