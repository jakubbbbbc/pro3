#include "player.h"

Player::Player(){}

void Player::setPos(int p){
    pos=p;
}

int Player::getPos(){
    return pos;
}

void Player:: makeMove(int dir){
    if (0==dir)
        --pos;
    else if (1==dir)
        ++pos;
}
