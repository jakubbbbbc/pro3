#ifndef BOARD_H
#define BOARD_H

#include<random>
#include<ctime>
#include "Player.h"
#include "Missile.h"
#include "Fifo.h"

static const int BoardWidth=31;
static const int BoardHeight=25;

class Board{
    int sizeX=BoardWidth;
    int sizeY=BoardHeight;
    int counter;            //for enemies speed
    int enSpeed;            //for enemies speed
    int highScore;
    int score;
    bool gameOver;
    char view[BoardHeight][BoardWidth];
    int enemies[BoardHeight][BoardWidth];
    int enShots[BoardHeight][BoardWidth];
    int dirCount;

public:
    Player p;
    Fifo<Missile> missiles;

    Board();
    void display();
    void moveEnemies();
    void enemyShoots();
    void moveShots();
    void update();
    bool missileHit(Missile*);
    void resetBoard(int); //int by zerowac punkty itp lub nie
    bool getGameOver();
};

#endif
