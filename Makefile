pro2: main.o Menu.o Fifo.o Node.o Point.o
	gcc main.o Menu.o Fifo.o Node.o Point.o -o pro2

main.o: main.cpp Menu.h
	gcc -c main.cpp

Menu.o: Menu.cpp Menu.h
	gcc -c Menu.cpp

Fifo.o: Fifo.cpp Fifo.h
	gcc -c Fifo.cpp

Node.o: Node.cpp Node.h
	gcc -c Node.cpp

Point.o: Point.cpp Point.h
	gcc -c Point.cpp