#ifndef NODE_H
#define NODE_H

template <class T>
class Node{
    public:
    T* el;
    Node* next;

    //methods
    Node(T *t){
        el=t;
        next=nullptr;
    }
};


#endif


