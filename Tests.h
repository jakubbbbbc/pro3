#ifndef TESTS_H
#define TESTS_H

#include<assert.h>
#include "Fifo.h"
#include "Missile.h"

class Tests{

    public:

    void declarationTest();
    void addGetTest();
    void clearTest();
    void deleteTest();
    void missileTest();

};

#endif
