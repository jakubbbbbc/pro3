#include "gamelauncher.h"

static int SleepTime = 50;

void beforeGame(){
    int c=0;
    while (0==GetAsyncKeyState(VK_SPACE)){
        std::cout<<"Welcome pilot!\n\n";
        std::cout<<"Let's save some universe, shall we!\n\n";
        if (0==c%2){
            std::cout<<"press space to continue";
            Sleep(500);
        }
        else
            Sleep(50);
        system("cls");
        ++c;
    }
}

/*void afterGame(){
    cout<<"you lost\n\n";
    cout<<"press P to continue";
    while (0==GetAsyncKeyState(0x50)){continue;}
}*/

void startGame(){
    Board* b=new Board();
    bool userQuits=false;
    while (!userQuits){
        while(!b->getGameOver()){
            system("cls");
            b->update();
            b->display();

            if (GetAsyncKeyState(0x50)!=0) //p key
                break;


            if (GetAsyncKeyState(VK_LEFT)!=0 && b->p.getPos()>1)
                b->p.makeMove(0);

            else if (GetAsyncKeyState(VK_RIGHT)!=0 && b->p.getPos()<BoardWidth-2)
                b->p.makeMove(1);

            else if (GetAsyncKeyState(VK_SPACE)!=0){
                Missile* m = new Missile(b->p.getPos(), BoardHeight-2);
                b->missiles.addNode(m);
            }

            Sleep(SleepTime);
        }
        system("cls");
        b->update();
        b->display();
        std::cout<<"You lost :(\n";
        std::cout<<"Press M to play again and beat your high score or P to quit";
        Sleep(2000);
        userQuits=true;
        bool spaceNotPressed=true;
        while (0==GetAsyncKeyState(0x50) && spaceNotPressed){
            if (0!=GetAsyncKeyState(0x4D)){
                spaceNotPressed=false;
                b->resetBoard(1);
                userQuits=false;
            }
        }
    }}
