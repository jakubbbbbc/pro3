#include "missile.h"

Missile::Missile(int x, int y):posX(x), posY(y){}

bool operator != (const Missile &c1, const Missile &c2){
    return (c1.posX!=c2.posX || c1.posY!=c2.posY);
}

std::ostream& operator << (std::ostream& os, const Missile& m){
    os<<"("<<m.posX<<", "<<m.posY<<")";
    return os;
}

int Missile::getPosX(){
    return posX;
}

int Missile::getPosY(){
    return posY;
}

void Missile::moveUp(){
        --posY;
}

bool Missile::collided(){
    return (posY<1);
}
