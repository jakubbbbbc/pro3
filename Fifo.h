#ifndef FIFO_H
#define FIFO_H

#include<iostream>
#include "Node.h"

template <class T>
class Fifo{
    public:
    Node<T>* head;
    Node<T>* tail;
    int listSize;

    public:

    Fifo();
    int getListSize();
    void addNode(T* t);
    void getNode();
    void clearList();
    Fifo& operator += (T *t);
    void deleteNode(Node<T>* n);
    void print();
    //template<T>
//    friend std::ostream& operator<< (std::ostream& , const Fifo<T>&);

};

template <class T>
Fifo<T>::Fifo(){
    head=nullptr;
    tail=nullptr;
    listSize=0;
}

template <class T>
int Fifo<T>::getListSize(){
    return listSize;
}

template <class T>
void Fifo<T>::addNode(T* t){
        *(this)+=t;
}

template <class T>
void Fifo<T>::getNode(){
    if (0==listSize){
        std::cout<<"No elements\n";
    }
    else{
        //std::cout<<*(head->el)<<"\n";
        Node<T>* pom=head;
        head=head->next;
        delete pom;
        if (listSize==1)
            tail=head;
        --listSize;
    }
}

template <class T>
Fifo<T>& Fifo<T>::operator += (T *t){
    Node<T>* newNode = new Node<T>(t);
    if (0==listSize){
        tail=newNode;
        head=newNode;
    }
    else{
        tail->next=newNode;
        tail=newNode;
    }
    ++listSize;
    return (*this);
}

/*template <class T>
ostream& operator << (ostream& os, Fifo<T> f){
    if (0==f.getListSize()){
        os<<"No elements";
    }
    else{
        Node<T>* temp=f.head;
        for(int i=0; i<f.listSize; ++i){
        os<<i+1<<". "<<*(temp->el)<<"\n";
        temp=temp->next;
        }
    }
    return os;
}*/

template <class T>
void Fifo<T>::clearList(){
    Node<T>* pom;
    while (listSize>0){
        pom=head;
        head=head->next;
        delete pom;
        --listSize;
    }
    tail=head;
}

template <class T>
void Fifo<T>::deleteNode(Node<T>* n){
    Node<T>* temp = head;
    Node<T>* prev = nullptr;
    while(*(temp->el)!=*(n->el) && temp->next){
        prev=temp;
        temp=temp->next;
    }
    //std::cout<<*(temp->el);
    if (prev==nullptr)
        head=head->next;
    else
        prev->next=temp->next;
    delete n;
    --listSize;

}

template <class T>
void Fifo<T>::print(){
    if (0==listSize){
        std::cout<<"No elements";
    }
    else{
        Node<T>* temp=head;
        for(int i=0; i<listSize; ++i){
        std::cout<<i+1<<". "<<*(temp->el)<<"\n";
        temp=temp->next;
        }
    }
}

#endif

